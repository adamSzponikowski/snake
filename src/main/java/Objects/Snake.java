package Objects;

import Core.GameScreen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.ArrayList;

public class Snake {
    public final ArrayList<Square> body = new ArrayList<>();
    public Square head;
    private float dx, dy;
    public Snake(int x, int y) {
        head = new Square(x, y, Color.BLUE);
        body.add(head);
    }
    public void draw(ShapeRenderer shapeRenderer) {
        for (Square square : body) {
            square.draw(shapeRenderer);
        }
    }

    private void settingDirection() {

        if ((Gdx.input.isKeyPressed(Input.Keys.A) && dx < 1)) {
            dy = 0;
            dx = -GameScreen.getSquare_size();
        } else if ((Gdx.input.isKeyPressed(Input.Keys.D) && dx > -1)) {
            dy = 0;
            dx = GameScreen.getSquare_size();
        } else if ((Gdx.input.isKeyPressed(Input.Keys.W)) && dy > -1) {
            dx = 0;
            dy = GameScreen.getSquare_size();
        } else if ((Gdx.input.isKeyPressed(Input.Keys.S)) && dy < 1) {
            dx = 0;
            dy = -GameScreen.getSquare_size();
        }
    }


    private void moveSnake() {
        float new_x = head.x + dx;
        float new_y = head.y + dy;
        head = new Square(new_x, new_y, Color.YELLOW);
        body.add(0, head);
    }

    public void update(Food food) {
        settingDirection();
        if (!checkCollideWithFood(food)) {
            body.remove(body.size() - 1);
        }
        moveSnake();
    }

    private boolean checkCollideWithFood(Food food) {
        if (head.overlaps(food)) {
            food.renew(body);
            return true;
        }
        return false;
    }

    public int checkGameEnd() {
        if (head.x == GameScreen.WIDTH || head.x < 0 || head.y == GameScreen.HEIGHT || head.y < 0) {
            return 1;
        }
        for (Square s : body.subList(1, body.size())) {
            if (head.overlaps(s)) {
                return 1;
            }
        }
        return 0;
    }

}
