package Objects;

import Core.GameScreen;
import com.badlogic.gdx.graphics.Color;

import java.util.List;

public class Food extends Square {

    public Food(List<Square> snake) {
        super(0, 0, Color.RED);
        renew(snake);
    }

    public void renew(List<Square> snake) {
        Point pos = findNewPos(snake);
        x = pos.x;
        y = pos.y;
    }

    public Point findNewPos(List<Square> snake) {
        int x_grid = GameScreen.WIDTH / GameScreen.getSquare_size();
        int y_grid = GameScreen.HEIGHT / GameScreen.getSquare_size();
        int x = ((int) (Math.random() * x_grid)) * GameScreen.getSquare_size();
        int y = ((int) (Math.random() * y_grid)) * GameScreen.getSquare_size();

        for (Square s : snake) {
            if (s.x == x && s.y == y) {
                return this.findNewPos(snake);
            }
        }
        return new Point(x, y);
    }
}