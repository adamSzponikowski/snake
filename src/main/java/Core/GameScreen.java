package Core;

import Objects.Food;
import Objects.Snake;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;

import static java.lang.Thread.sleep;

public class GameScreen extends ScreenAdapter {

    public static int WIDTH = 800;
    public static int HEIGHT = 800;
    private ShapeRenderer shapeRenderer;
    private static int square_size = 20;
    private Snake snake;
    private OrthographicCamera orthographicCamera;
    private Food food;

    int gameEnd = 0;

    public GameScreen(OrthographicCamera orthographicCamera) {
        this.orthographicCamera = orthographicCamera;
        shapeRenderer = new ShapeRenderer();
        snake = new Snake(square_size, square_size);
        food = new Food(snake.body);
    }

    public void update() {
        int delay;
        delay = 100;
        snake.update(food);
        gameEnd = snake.checkGameEnd();
        stagger(delay);
        shapeRenderer.setProjectionMatrix(orthographicCamera.combined);
        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            Gdx.app.exit();
        }
    }

    @Override
    public void render(float delta) {
        this.update();
        if (gameEnd == 1) {
            Gdx.app.exit();
        }
        clearScreen();
        shapeRenderer.begin(ShapeType.Filled);
        snake.draw(shapeRenderer);
        food.draw(shapeRenderer);
        shapeRenderer.end();
    }

    @Override
    public void dispose() {
        shapeRenderer.dispose();
    }


    public void clearScreen() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    public static int getSquare_size() {
        return square_size;
    }

    private void stagger(int delay) {
        try {
            sleep(delay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
