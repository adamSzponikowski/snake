package Core;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

public class DesktopLauncher {
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration configuration = new Lwjgl3ApplicationConfiguration();
        configuration.setTitle("Snake game");
        configuration.useVsync(true);
        configuration.setWindowedMode(800, 800);
        new Lwjgl3Application(new SnakeGame(), configuration);
    }
}
